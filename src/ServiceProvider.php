<?php

namespace KDA;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{


    use \KDA\Laravel\Traits\HasMigration;

    
   // protected $shouldLoadMigrations = true;
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
}
