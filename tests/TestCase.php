<?php

namespace KDA\Tests;

use KDA\ServiceProvider;
use Orchestra\Testbench\TestCase as ParentTestCase;

class TestCase extends ParentTestCase
{
  public function setUp(): void
  {
    parent::setUp();
  }

  protected function getPackageProviders($app)
  {
    return [

      ServiceProvider::class,
    ];
  }

  protected function defineDatabaseMigrations()
  {
    /*
    $this->artisan('migrate', ['--database' => 'mysql'])->run();
    $this->beforeApplicationDestroyed(function () {
      $this->artisan('migrate:rollback', ['--database' => 'mysql'])->run();
    });
    */
    //  $this->artisan('vendor:publish', ['--provider'=>ServiceProvider::class,'--tag' => 'migrations','--force'=>true])->run();
  }


/*  protected function loadMigration($migration)
  {
    $classes = get_declared_classes();
    include_once __DIR__ . '/../database/migrations/' . $migration;
    $diff = array_diff(get_declared_classes(), $classes);
  
    $class = reset($diff);
    
    (new $class)->up();
  }
*/
  protected function getEnvironmentSetUp($app)
  {
    $provider = $this->getPackageProviders($app)[0];
    $traits = class_uses_recursive($provider);
    if (in_array('KDA\Laravel\Traits\HasLoadableMigration', $traits)) {
      $this->artisan('migrate', ['--database' => 'mysql'])->run();
      $this->beforeApplicationDestroyed(function () {
        $this->artisan('migrate:rollback', ['--database' => 'mysql'])->run();
      });
    } else {
  //    $this->loadMigration('create_carts_table.php');

        include_once __DIR__ . '/../database/migrations/' . $migration;
    }
  }
}
